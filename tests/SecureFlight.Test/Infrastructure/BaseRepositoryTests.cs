using Moq;
using Xunit;
using SecureFlight.Infrastructure.Repositories;
using SecureFlight.Infrastructure;

namespace SecureFlight.Test;

public class BaseRepositoryTests {
    private readonly Mock<SecureFlightDbContext> _secureFlightDbContext;
    private readonly BaseRepository<DummyEntity> _sut;

    public BaseRepositoryTests()
    {
        _secureFlightDbContext = new Mock<SecureFlightDbContext>();     
        _sut = new BaseRepository<DummyEntity>(_secureFlightDbContext.Object);
    }

    [Fact]
    public void GivenAndEntity_WhenIsUpdated_MustSaveChanges()
    {
        // Given
    
        // When
        var result = _sut.Update(new DummyEntity());
    
        // Then
        _secureFlightDbContext.Verify(
            dbDontext => dbDontext.SaveChanges(),
            Times.Once);
    }
}

public record DummyEntity();